package com.lamark.voicerecognitionpoc.Utils

import android.app.Activity
import android.util.Log
import java.io.InputStream


/**
 * Created by Pavel.k on 12/18/2018.
 */
class AssetManager(context: Activity) {
    companion object {
        const val IMAGES = 1
        const val SOUNDS = 2
        const val RAW_NAMES = 3
    }

    private var images: Array<out String>? = context.assets.list("pictures");
    private var sounds: Array<out String>? = context.assets.list("speeches");

    fun printAsstets(){
        images!!.forEach {
            Log.i("Images: ", it);
        }
        sounds!!.forEach {
            Log.i("Sounds",it);
        }

    }

   public fun getItemList(type: Int): Array<out String>? {
        when(type){
            IMAGES -> {
                return images;
            }

            SOUNDS ->{
                return sounds;
            }

            else -> {
                return null
            }
        }

    }

   public fun getAssetsLength():Int {
        return images!!.size;
    }
}