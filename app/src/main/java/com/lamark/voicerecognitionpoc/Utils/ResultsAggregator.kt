package com.lamark.voicerecognitionpoc.Utils

import android.content.Context
import android.net.Uri
import android.os.Environment
import android.util.Log
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.PrintWriter

/**
 * Created by Pavel.k on 12/19/2018.
 */
class ResultsAggregator (context: Context){
    private val fileName = "VoicePOCResults"
    private val savePath = "${context.externalCacheDir.absolutePath}"
    private val resultsCache = HashMap<String,String>();
    private val mContext = context;
    private val PRINTING_TAG = this.javaClass.simpleName



    fun cacheResult(category:String,result:String){
        resultsCache.put(category,result)
    }

    private fun AggregateResultsData():String{
        var resultsContent = ""
        resultsCache.forEach {
            resultsContent += "${it.key} ---------------------- ${it.value}\n"
        }
        return resultsContent
    }

    fun saveResultsFile(){
        val sd_main = File("${mContext.externalCacheDir.absolutePath}","VoicePOC")
        sd_main.mkdirs()
        val file = File(sd_main,fileName + ".txt")
        FileOutputStream(file).use {
            it.write(AggregateResultsData().toByteArray())
        }


    }

    fun getResultsFile():String{
        return File("${mContext.externalCacheDir.absolutePath}/VoicePOC","${fileName}.txt").absolutePath
    }



}