package com.lamark.voicerecognitionpoc

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.media.AudioFormat
import android.media.MediaRecorder
import android.support.v7.app.AppCompatActivity
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.media.AudioRecord
import android.os.*
import android.support.design.widget.FloatingActionButton
import android.support.v4.view.ViewPager
import com.lamark.voicerecognitionpoc.Fragments.DemoCollectionPagerAdapter
import com.lamark.voicerecognitionpoc.Fragments.QuizFragment
import com.lamark.voicerecognitionpoc.Interfaces.IResponseHandler
import com.lamark.voicerecognitionpoc.Utils.AssetManager
import com.lamark.voicerecognitionpoc.Utils.ResultsAggregator
import com.lamark.voicerecognitionpoc.Utils.WavRecorder
import java.io.File
import java.io.Reader
import java.util.*
import kotlin.concurrent.timerTask


class MainActivity : AppCompatActivity(), IResponseHandler {





    val RECORD_PERMISION_CODE: Int  = 200;
    var SAMPLE_RATE: Int = 16000;
    var mHasRecordPermission: Boolean = false;
    private var mFileName: String = ""
    private var mRecorder: MediaRecorder? = null
    private var mIsRecording: Boolean = false;
    var wavRecorder: WavRecorder? = null
    var mNetworkManager: NetworkManager? = null;
     lateinit var mDemoCollectionPagerAdapter: DemoCollectionPagerAdapter
    private lateinit var mViewPager: ViewPager
    private lateinit var mAssetManager:AssetManager
    private lateinit var mResultsAggr: ResultsAggregator
    private  var mIsLoading = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mViewPager = findViewById(R.id.mainViewPager)
        mAssetManager = AssetManager(this)
        mResultsAggr = ResultsAggregator(this)
        mDemoCollectionPagerAdapter = DemoCollectionPagerAdapter(supportFragmentManager,mAssetManager)
        mViewPager.adapter = mDemoCollectionPagerAdapter

        mFileName = "${externalCacheDir.absolutePath}/audiorecordtest.wav";
        if(!mHasRecordPermission){
            requestPermissions();
        }

        if(mNetworkManager == null)mNetworkManager = NetworkManager(this);


    }

   public fun getResultsAggregator():ResultsAggregator{
        return mResultsAggr
    }


    fun startRecording(){
            findViewById<FloatingActionButton>(R.id.floatingRecordButton).apply {
                setImageResource(android.R.drawable.ic_media_pause)
            }
        val vibratorService = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibratorService.vibrate(VibrationEffect.createOneShot(300,VibrationEffect.DEFAULT_AMPLITUDE))
        }else{
            vibratorService.vibrate(300)
        }

            mRecorder =  MediaRecorder().apply {
                setAudioSource(MediaRecorder.AudioSource.MIC);
                setAudioSamplingRate(SAMPLE_RATE)
                setOutputFormat(AudioFormat.ENCODING_PCM_16BIT);
                setOutputFile(mFileName);
                setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            }


             wavRecorder = WavRecorder(mFileName);
            wavRecorder!!.startRecording();
            mIsRecording = true;
            Snackbar.make(window.decorView.findViewById(android.R.id.content),"Start recording",Snackbar.LENGTH_SHORT).show();
            val timer = Timer()
            timer.schedule(timerTask {
                runOnUiThread {
                    if (mIsRecording) {
                        stopRecording()
                    }
                }
            },3000)

    }

    fun stopRecording(){
        findViewById<FloatingActionButton>(R.id.floatingRecordButton).apply {
            setImageResource(android.R.drawable.ic_btn_speak_now)
        }
       Snackbar.make(window.decorView.findViewById(android.R.id.content),"Stopped Recording",Snackbar.LENGTH_SHORT).show();
        wavRecorder!!.stopRecording();
        mIsRecording = false;
        mRecorder = null;
        Log.i("Record log","Recorded to " + mFileName);
        mNetworkManager!!.sendFileToSoapbox(File(mFileName), mAssetManager.getItemList(AssetManager.IMAGES)!!.get(mViewPager.currentItem).replace(".jpg",""));
        Log.i("Record log","Category: ${mAssetManager.getItemList(AssetManager.IMAGES)!!.get(mViewPager.currentItem)}")

    }



    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            RECORD_PERMISION_CODE -> {
                mHasRecordPermission = (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            }
            else -> {
                mHasRecordPermission = false;
            }
        }
    }

    fun requestPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.RECORD_AUDIO), RECORD_PERMISION_CODE)
        }else{
            mHasRecordPermission = true;
        }
    }

     fun toggleRecording(view: View) {
        if (mHasRecordPermission) {
            if (mRecorder == null) {
                startRecording()
            } else {
                stopRecording();
            }
        }
    }

    public fun getIsLoading():Boolean{
        return mIsLoading;
    }

    override fun SoapBoxResponse(category: String?,resultScore :String?) {
        Log.i("Soapbox response","Category: ${category} ResultScore: ${resultScore}")
        if (category != null) {
            if (resultScore != null) {
                runOnUiThread {
                    mDemoCollectionPagerAdapter.updateScores(category,resultScore,mViewPager.currentItem)
                }
                mResultsAggr.cacheResult(category,resultScore)

            }
        }
        mIsLoading = false
    }

    override fun FinishedWithFailure() {
        Snackbar.make(window.decorView.findViewById(android.R.id.content),"Network failure, please try again later",Snackbar.LENGTH_SHORT).show();
        mIsLoading = false
        mDemoCollectionPagerAdapter.notifyDataSetChanged()
    }

    override fun SendingRequest() {
        mIsLoading = true
        mDemoCollectionPagerAdapter.notifyDataSetChanged()
    }

}
