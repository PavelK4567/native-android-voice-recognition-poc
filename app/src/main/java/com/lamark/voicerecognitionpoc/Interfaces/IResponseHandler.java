package com.lamark.voicerecognitionpoc.Interfaces;

/**
 * Created by Pavel.k on 12/17/2018.
 */

public interface IResponseHandler {
    void SoapBoxResponse(String category,String resultScore);
    void SendingRequest();
    void FinishedWithFailure();
}
