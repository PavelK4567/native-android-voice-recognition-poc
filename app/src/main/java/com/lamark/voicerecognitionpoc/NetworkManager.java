package com.lamark.voicerecognitionpoc;

import android.support.design.widget.Snackbar;
import android.util.Log;

import com.google.gson.Gson;
import com.lamark.voicerecognitionpoc.Interfaces.IResponseHandler;
import com.lamark.voicerecognitionpoc.Model.SoapBoxResponseModel;
import com.lamark.voicerecognitionpoc.Utils.SoapBoxAPI;

import java.io.File;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Pavel.k on 12/17/2018.
 */

public class NetworkManager {

    OkHttpClient client;
     String mSoapBoxSpeechUrl = SoapBoxAPI.SOAPBOX_BASEURL + SoapBoxAPI.SOAPBOX_SPEECHAPI;
    final String NETWORK_RESPONSE_TAG = "Network response: ";
    final String NETWORK_ERROR_TAG = "Network error: ";
    private IResponseHandler iResponseHandler;
    public NetworkManager(IResponseHandler iResponseHandler) {
        client = new OkHttpClient();
        this.iResponseHandler = iResponseHandler;
    }

    public void sendFileToSoapbox(File file,String categoryWord){
            iResponseHandler.SendingRequest();
        final RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("category",categoryWord)
                .addFormDataPart("user_token","lamark111aaa")
                .addFormDataPart("file",file.getName(),RequestBody.create(MediaType.parse("audio/wav"),file))
                .build();

        Request request = new Request.Builder()
                .url(mSoapBoxSpeechUrl)
                .header("X-App-Key",SoapBoxAPI.SOAPBOX_APPKEY)
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(NETWORK_ERROR_TAG,e.getMessage());
                iResponseHandler.FinishedWithFailure();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()){
                    Log.e(NETWORK_ERROR_TAG,response.message());
                    iResponseHandler.FinishedWithFailure();
                }else{
                    Gson gson = new Gson();
                    SoapBoxResponseModel soapBoxResponseModelArray = gson.fromJson(response.body().charStream(),SoapBoxResponseModel.class);
                    iResponseHandler.SoapBoxResponse(soapBoxResponseModelArray.getCategoryWord(),soapBoxResponseModelArray.getResultScore());
                 //   Log.i(NETWORK_RESPONSE_TAG,soapBoxResponseModelArray.toString() + soapBoxResponseModelArray.getCategoryWord());
                }
            }
        });
    }
}
