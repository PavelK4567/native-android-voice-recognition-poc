package com.lamark.voicerecognitionpoc.Fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.lamark.voicerecognitionpoc.MainActivity
import com.lamark.voicerecognitionpoc.R
import com.lamark.voicerecognitionpoc.Utils.ResultsAggregator

/**
 * Created by Pavel.k on 12/19/2018.
 */
class QuizFragmentLast: Fragment() {

    lateinit var mContext:Context
    lateinit var mResultsAggrInstance:ResultsAggregator
    val mRecipients = arrayListOf("david.t@la-mark.com")
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context!!
        mResultsAggrInstance = (activity as MainActivity).getResultsAggregator()

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.carousel_view_last,container,false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view!!.findViewById<ImageButton>(R.id.carouselSendButton).setOnClickListener {
            sendResultsByEmail()
        }
    }

    fun sendResultsByEmail(){
        mResultsAggrInstance.saveResultsFile()
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto:david.t@la-mark.com")
        intent.putExtra(Intent.EXTRA_EMAIL,mRecipients)
        intent.putExtra(Intent.EXTRA_SUBJECT,"Voice POC Results")
        intent.putExtra(Intent.EXTRA_STREAM,Uri.parse("file://" +mResultsAggrInstance.getResultsFile()))
        if(intent.resolveActivity(activity.packageManager) != null){
            startActivity(intent)
        }
    }
}