package com.lamark.voicerecognitionpoc.Fragments

import android.content.res.AssetFileDescriptor
import android.graphics.drawable.Drawable
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.lamark.voicerecognitionpoc.Interfaces.IResponseHandler
import com.lamark.voicerecognitionpoc.MainActivity
import com.lamark.voicerecognitionpoc.R
import java.io.FileNotFoundException
import java.io.InputStream

/**
 * Created by Pavel.k on 12/18/2018.
 */
 class QuizFragment : Fragment() {



    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.carousel_view,container,false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var pos = arguments?.getInt("position")
        view?.findViewById<TextView>(R.id.carouselText).apply {
            this!!.text = arguments?.getString("word")
        }
        var ims:InputStream = context.assets.open("pictures/" + arguments.getString("image"))
        var d:Drawable = Drawable.createFromStream(ims,null)
        view?.findViewById<ImageView>(R.id.carouselImage)!!.setImageDrawable(d);
        view?.findViewById<ImageButton>(R.id.carouselPlayButton)!!.setOnClickListener(View.OnClickListener {
            try {
                val afd:AssetFileDescriptor = context.assets.openFd("speeches/" + arguments.getString("sound"))

                val mediaPlayer: MediaPlayer? = MediaPlayer().apply {
                    setDataSource(afd.fileDescriptor,afd.startOffset,afd.declaredLength)
                    prepare()
                    start()
                }
            }catch (e:FileNotFoundException){
                Snackbar.make(view,"File ${arguments?.getString("word")} cannot be played",Snackbar.LENGTH_SHORT).show()
                Log.e("AssetError","File ${arguments?.getString("sound")} cannot be played, " + e.message)
            }

        })
        //TODO Fix it
        if((activity as MainActivity).mDemoCollectionPagerAdapter.mScores.get(arguments?.getString("word")!!) != null && (activity as MainActivity).mDemoCollectionPagerAdapter.mScores.get(arguments?.getString("word")!!)!!.length > 1){
            view?.findViewById<TextView>(R.id.carouselTextScore).apply {
                this!!.text = "Score for this word: " + (activity as MainActivity).mDemoCollectionPagerAdapter.mScores.get(arguments?.getString("word")!!)
            }
        }

        var mIsLoading = (activity as MainActivity).getIsLoading()
        if(mIsLoading){
            view.findViewById<ProgressBar>(R.id.loader).visibility = View.VISIBLE
        }else{
            view.findViewById<ProgressBar>(R.id.loader).visibility = View.INVISIBLE
        }




    }







}