package com.lamark.voicerecognitionpoc.Fragments

import android.app.FragmentManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.view.ViewGroup
import com.lamark.voicerecognitionpoc.Utils.AssetManager
import com.lamark.voicerecognitionpoc.Utils.ResultsAggregator

/**
 * Created by Pavel.k on 12/18/2018.
 */
class DemoCollectionPagerAdapter(fm: android.support.v4.app.FragmentManager?,assetManager: AssetManager) : FragmentPagerAdapter(fm) {

    val mAssetManager = assetManager
    var mScores: HashMap<String,String> = HashMap();

    override fun getItem(position: Int): Fragment {
        when(position){
            count -1 -> {
                var lastFragment = QuizFragmentLast();
                return lastFragment
            }
            else ->{
                var quizIns = QuizFragment();
                quizIns.arguments = Bundle().apply {
                    val currentWord = mAssetManager.getItemList(AssetManager.IMAGES)!!.get(position).replace(".jpg","")
                    putString("word", currentWord)
                    putString("image", mAssetManager.getItemList(AssetManager.IMAGES)!!.get(position))
                    putString("sound", mAssetManager.getItemList(AssetManager.SOUNDS)!!.get(position))
                    if(mScores.get(currentWord) != null){
                        putString("score",mScores.get(mAssetManager.getItemList(AssetManager.IMAGES)!!.get(position).replace(".jpg","")).toString())
                    }else{
                        putString("score","")
                    }

                }
                return quizIns;
            }
        }

    }

    override fun getCount(): Int {
        return mAssetManager.getAssetsLength() +1
    }

    override fun getItemPosition(`object`: Any?): Int {
        return PagerAdapter.POSITION_NONE
    }



    public fun updateScores(category:String, score:String, position: Int){
        mScores.put(category,score)
        notifyDataSetChanged()
    }


}