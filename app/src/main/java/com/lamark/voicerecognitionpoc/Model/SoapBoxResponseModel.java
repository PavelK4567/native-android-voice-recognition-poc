package com.lamark.voicerecognitionpoc.Model;

import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.LinkedTreeMap;

/**
 * Created by Pavel.k on 12/17/2018.
 */

public class SoapBoxResponseModel {
  @SerializedName("results")
  public LinkedTreeMap[] results = {

  };


  public String filename,time,userToken,url,languageCode;

  public String getCategoryWord(){
    return  results[0].get("category").toString();
  }


  public String getResultScore(){
    return results[0].get("hypothesis_score").toString();
  }
}
